const express = require('express');
const { Client } = require('@opensearch-project/opensearch');
const mysql = require('mysql2');

const app = express();
const port = 8080;

// Konfigurasi koneksi ke OpenSearch
const opensearch_host = '110.239.67.138'
const mysql_host = '54.167.54.9'
// const opensearch_host = 'opensearch-node1'
// const mysql_host = 'mysql-serv'

const opensearchClient = new Client({
  node: `https://admin:admin@${opensearch_host}:9200`, // Ganti dengan URL OpenSearch Anda
  ssl: {
    rejectUnauthorized: false
  },
});

// Konfigurasi koneksi ke MySQL
const poolMysql = mysql.createPool({
	// host: `${process.env.MYSQL_HOST}` || 'localhost', // Ganti dengan host MySQL Anda
	host: `${mysql_host}`, // Ganti dengan host MySQL Anda
	port: '3306',
	user: 'root', // Ganti dengan username MySQL Anda
  password: 'temp123', // Ganti dengan password MySQL Anda
  database: 'movies', // Ganti dengan nama database MySQL Anda
});

const indexName = 'movies'; // Ganti dengan nama indeks yang Anda gunakan

// Fungsi untuk memeriksa apakah indeks sudah ada
async function checkIndexExists() {
  try {
    const existsResponse = await opensearchClient.indices.exists({
      index: indexName,
    });
    return existsResponse.body;
  } catch (error) {
    console.error('Gagal memeriksa keberadaan indeks:', error);
    throw error;
  }
}

// Fungsi untuk membuat indeks jika belum ada
async function createIndexIfNotExists() {
  const indexExists = await checkIndexExists();
  if (!indexExists) {
    try {
      await opensearchClient.indices.create({
        index: indexName,
      });
      console.log(`Indeks ${indexName} telah dibuat.`);
    } catch (error) {
      console.error('Gagal membuat indeks:', error);
      throw error;
    }
  }
}


// Fungsi untuk membuat indeks jika belum ada
async function retrieveOpensearchData(querydata) {
  const response = await opensearchClient.search({
    index: 'movies', // Ganti dengan nama indeks OpenSearch Anda
    body: {
      query: {
        wildcard: {
          title: `*${querydata.toLowerCase()}*`,
        },
      },
    },
  });
  return response;
}

app.get('/search', async (req, res) => {
  const searchTerm = req.query.searchTerm;

  await createIndexIfNotExists();

  try {
    // Cari di OpenSearch
    let date_ob = new Date();
    let minutes = date_ob.getMinutes();
    let identifier = 2 // plan is per 15 minutes 
    if(minutes%identifier==0){
      console.log(`Now is ${minutes}`)
    }
    // const opensearchResponse = await opensearchClient.search({
    //   index: 'movies', // Ganti dengan nama indeks OpenSearch Anda
    //   body: {
    //     query: {
    //       match: {
    //         title: searchTerm,
    //       },
    //     },
    //   },
    // });
    
    const opensearchResponse = await retrieveOpensearchData(searchTerm)
    
    const valuehits = opensearchResponse.body.hits.total.value
    console.log(`hits value : ${valuehits}`)

    if (opensearchResponse.body.hits.total.value > 0) {
      // Jika ditemukan di OpenSearch, kirim hasilnya
      const opensearchHits = opensearchResponse.body.hits.hits;
      console.log(`Getting Data from Elasticsearch`)
      res.json(opensearchHits);
    } else {
      console.log(`Getting Data from Database`)
      // Jika tidak ditemukan di OpenSearch, ambil dari MySQL
      poolMysql.query(
        `SELECT * FROM listmovies WHERE title LIKE '%${searchTerm}%'`,
        (error, results) => {
          if (error) throw error;
          
          // Simpan data ke OpenSearch
          results.forEach(
            async (movie) => {
            await opensearchClient.index({
              index: 'movies', // Ganti dengan nama indeks OpenSearch Anda
              body: movie,
            });
          });

          // for (const prop in results) {
          //   console.log(`${prop}: ${results[prop]}`);
          //   const moviedata = results[prop]
          //   for (const prop1 in moviedata) {
          //     console.log(`${prop1}: ${moviedata[prop1]}`);
          //   }
          // }
          
          // res.json(results);
        }
      );
      
      console.log(`Finished Get Data from Database, Get data from Opensearch`)
      const opensearchResponseMysql = await retrieveOpensearchData(searchTerm)
      const opensearchHitsMysql = opensearchResponseMysql.body.hits.hits;
      res.json(opensearchHitsMysql);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`Server berjalan di http://localhost:${port}`);
});
