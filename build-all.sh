git pull
docker stop dockerized-nodejs-opensearch
docker rm dockerized-nodejs-opensearch
docker build -t dockerized-nodejs-opensearch . --no-cache
docker run -d --name dockerized-nodejs-opensearch -p 8080:8080 --network noos-network  dockerized-nodejs-opensearch
docker network connect opensearch_opensearch-net  dockerized-nodejs-opensearch
docker logs dockerized-nodejs-opensearch -f
