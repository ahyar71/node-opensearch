FROM node:16.20-alpine3.18

WORKDIR /usr/src/app

COPY package*.json ./
RUN cat package.json
RUN npm install

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "index.js" ]